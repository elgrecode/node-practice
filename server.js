var http = require('http');
var util = require('util');
var url = require('url');
var os = require('os');
var sniffer = require('./httpsniffer');

var server = http.createServer();
server.on('request', (req, res) => {
  var requrl = url.parse(req.url, true);
  console.log('req', requrl);
  if (requrl.pathname === '/'){
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end(
      `<html>
        <head>
          <title>Hello, world!</title>
        </head>
        <body>
          <h1>Hello, world!</h1>
          <p><a href='/osinfo'>OS Info</a></p>
        </body>
      </html>`
    );
  } else if (requrl.pathname === "/osinfo") {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end( `
      <html>
        <head>
          <title>Operating System Info</title>
        </head>
        <body>
          <h1>Operating System Info</h1>
          <p>${ os.tmpDir()}</p>
          <p>Host Name: <pre>${ os.hostname()}</pre></p>
          <p>OS Type <pre>${ os.type()} ${ os.platform()} ${ os.arch()} ${ os.release()}</pre></p>
          <p><pre>${ os.uptime()} ${ util.inspect( os.loadavg())}</pre></p>
          <p>Memory total: <pre>${ os.totalmem()} free: ${ os.freemem()}</pre></p>
          <p>CPU's <pre>${ util.inspect( os.cpus())}</pre> </p>
          <p>Network <pre>${ util.inspect( os.networkInterfaces())}</pre></p>
        </body>
      </html>`
    );
  } else {
    res.writeHead(404, {'Content-Type': 'text/plain'});
    res.end("bad URL " + req.url);
  }
});

sniffer.sniffOn(server);
server.listen(8124);
console.log('listening to http://localhost:8124');
