var math = require('./math');
var util = require('util');

for (var num = 1; num < 800; num++) {
  util.log(`Fibonacci for ${num} = ${math.fibonacciLoop(num)}`);
}
